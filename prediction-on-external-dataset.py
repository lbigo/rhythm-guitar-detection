"""
This script uses function_guitar.csv, therefore it requires
that features have been previsouly extracted by running extraction-features.py
"""
from numpy.lib.function_base import blackman
from feature_list import *
from utils_training import *
from utils_extraction_features import *
import pandas as pd

ext_dataset_path = '../msb-scripts/gpFiles_by_categories/standard' 

bidirectional_training_bool = True
recurrence_bar_number = 5

df = pd.read_table("./function_guitar.csv",sep = ',',header = 0)
number_of_bars_in_corpus = df.shape[0]
training_song_name_list = get_song_name_list(df)

MDFeatures = min_delta(df,active_features_list)

# Extract features of the external (non-annotated) dataset

def extract_features_ext():
    tableau_csv = [get_features_table_headers(with_labels=False)]
    gpif_dir = ext_dataset_path
    for file in sorted(os.listdir(gpif_dir)):
        if os.path.isfile(gpif_dir+'/'+file) and file.endswith('.gp'):
            score_gpif = gpif_dir+'/'+file.replace('.gp','')+'/score.gpif'
            s = ParserGP().parseFile(score_gpif)
            print("extract features from {}".format(score_gpif))
            title = s.metadata.title
            artist = s.metadata.getContributorsByRole('artist')[0].name
            titre = title+'\n'+artist
            parties = s.parts
            lp = len(parties)
            for i in range(lp) :
                partie = parties[i]
                nompartie = partie.partName

                num_mes = 0
                for mes in partie :
                    num_mes+=1
                    measure_features = extract_features_from_measure(mes)

                    if measure_features is not None : # non empty measure
                        tableau_csv.append([str(score_gpif), titre, nompartie, num_mes]+measure_features)
    # writing the csv file
    with open("./function_guitar-ext.csv", "w") as f_write:
        writer = csv.writer(f_write)
        for row in tableau_csv:
            writer.writerow(row)


# Train and store the model on the whole set of annotated files

def train_model():
    number_of_bars_in_corpus = df.shape[0]
    training_file_names = get_all_file_names(df)
    training_feature_values = [[] for i in range(len(training_file_names))] # one empty list for each training gpif file
    training_label_values = [[] for i in range(len(training_file_names))]
    training_bar_numbers = [[] for i in range(len(training_file_names))]
 
    for bar_n in range(number_of_bars_in_corpus) :
        file_name = df.at[bar_n,'Fichier']
        V = vecteur(bar_n,df,MDFeatures,active_features_list,label=True)  # [line_number,manual_label,normalize_feat_1,...]
        training_feature_values[training_file_names.index(file_name)].append(V[2:]) # For each training file (length of the list) : a list of bars represented as feature vectors
        training_label_values[training_file_names.index(file_name)].append(V[1])
        training_bar_numbers[training_file_names.index(file_name)].append(bar_n)

    model = None

    if bidirectional_training_bool:
        model = build_and_get_bidirectional_model(training_feature_values, training_label_values, training_bar_numbers,recurrence_bar_number)
        # model = load_model('modele.h5')
    else :
        model = build_and_get_unidirectional_model(training_feature_values,training_label_values,training_bar_numbers,recurrence_bar_number)
        # model = load_model('modele.h5')

    return model

# Predict on the external dataset et store the predictions in a csv file

def prediction(model):
    ext_dataset_df = pd.read_table("./function_guitar-ext.csv",sep = ',',header = 0)
    ext_file_names = get_all_file_names(ext_dataset_df)
    ext_feature_values = [[] for i in range(len(ext_file_names))] # one empty list for each ext gpif file
    ext_bar_numbers = [[] for i in range(len(ext_file_names))]
    number_of_bars_in_corpus = ext_dataset_df.shape[0]
    for bar_n in range(number_of_bars_in_corpus) :
        file_name = ext_dataset_df.at[bar_n,'Fichier']
        V = vecteur(bar_n,ext_dataset_df,MDFeatures,active_features_list,label=False)  # [line_number,normalize_feat_1,...]
        ext_feature_values[ext_file_names.index(file_name)].append(V[1:]) # For each ext file (length of the list) : a list of bars represented as feature vectors
        ext_bar_numbers[ext_file_names.index(file_name)].append(bar_n)
    forward_ext_feature_values = build_cons_forward_bars_groups(ext_feature_values,ext_bar_numbers, n=recurrence_bar_number)
    if bidirectional_training_bool:
        backward_ext_feature_values = build_cons_backward_bars_groups(ext_feature_values,ext_bar_numbers, n=recurrence_bar_number)
        predicted_labels = model.predict({'forward_groups':forward_ext_feature_values,'backward_groups':backward_ext_feature_values})
    else :
        predicted_labels = model.predict(np.array(forward_ext_feature_values))
    predicted_labels = [p[0] for p in predicted_labels]
    ext_dataset_df['rg-estimation'] = predicted_labels
    ext_dataset_df['Fichier'] = file_name.split('/')[-2] # file name instead of score.gpif path
    ext_dataset_df.to_csv('./function_guitar-ext-with-predictions.csv')

extract_features_ext()
model = train_model()
prediction(model)