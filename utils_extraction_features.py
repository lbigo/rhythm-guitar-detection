#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 10 09:19:06 2020

@author: amusic
"""

from music21 import *
from parserGP import ParserGP
from fractions import Fraction
from math import log2, log
import os
import glob
from zipfile import ZipFile
import csv


"""RéCUPéRATION DE TOUTES LES MESURES D'UN CORPUS DE FICHIERS
PUIS TRI ENTRE MESURES SOLOS OU MESURES D'ACCOMPAGNEMENT

CRéATION D'UN FICHIER CSV AVEC LES FEATURES QUE L'ON CONSERVE
"""

"""Si on a plusieurs lyrics en même temps, (accord+letring+fun:acc par exemple)
la chaîne de caractère récupérée est ['L', 'e', 't', 'R', 'i', 'n', 'g', '\n', 'f', 'u', 'n', ':', 'a', 'c', 'c', '\n', 'B', 'm']
'LetRing\nfun:acc\nBm' ... il faut donc regarder si au sein des paroles on a fun:acc ou fun:solo"""

def get_features_table_headers(with_labels):
    headers = ["Fichier","Track_Name","Partie","Mesure"]
    if with_labels:
        headers = headers + ["Solo","Acc"]
    headers = headers + ["Has_SingleNotes","Has_Chords","Has_2_note_chords",
                    "Has_3_note_chords","Has_4_note_chords","Has_5_note_chords","Has_6_note_chords","Nb_Notes_Total",
                    "Midi_moy","Midi_min","Midi_max","Ambitus","Min_string","Max_string","String_moy","Min_fret","Max_fret","Fret_moy","Div_durees","Div_notes","Div_chords","Intervalle_min",
                    "Intervalle_max","Div_intervalles","Nb_Letring", "Letring_Present", "Letring100pourcents",
                    "Nb_VibratoW","Presence_VibratoMG","Nb_Whammy_Bar","Presence_bends",
                    "has_triad","has_fifth"]
    return headers


def extract_features_from_measure(mes):
    div_notes, div_chords, div_notes_total, nb_notes, nb_chords, nb_notes_total = diversite_notes_et_chords(mes)

    has_single_notes = 1 if nb_notes > 0 else 0
    has_chords = 1 if nb_chords > 0 else 0

    if nb_notes_total > 0 :
        """Calcul des autres features"""
        div_rythm = diversite_durees(mes)
        midi_moy, midi_min, midi_max = val_midi(mes)
        ambitus = midi_max-midi_min
        itv_max, itv_min, div_itv = intervalles(mes)
        taille_acc = tailles_accords(mes)

        min_string,max_string,mean_string,min_fret,max_fret,mean_fret = get_string_fret_stats_from_measure(mes)

        has_2_note_chords = 1 if taille_acc[1] > 0 else 0
        has_3_note_chords = 1 if taille_acc[2] > 0 else 0
        has_4_note_chords = 1 if taille_acc[3] > 0 else 0
        has_5_note_chords = 1 if taille_acc[4] > 0 else 0
        has_6_note_chords = 1 if taille_acc[5] > 0 else 0

        vibratoW, vibratoMG, whammy, bend = bend_et_vibrato(mes)

        presence_vibratoMG = 1 if vibratoMG > 0 else 0
        presence_bends = 1 if bend > 0 else 0

        #Features sur le letring
        nb_letring = letring(mes)
        presence_letring = int(nb_letring!=0)
        letring100pourcents = int(nb_letring==nb_notes+nb_chords)

        #Détection de la présence ou non de powerchord, triademin ou triademaj
        #sur les 4 notes les plus fréquentes
        # powerchord, triademaj, triademin = detection_triade(mes)
        presence_fifth,presence_triad = presence_fifth_and_triad(mes)
        presence_fifth = 1 if presence_fifth else 0
        presence_triad = 1 if presence_triad else 0

        return [has_single_notes,has_chords, has_2_note_chords,
                has_3_note_chords, has_4_note_chords, has_5_note_chords,
                has_6_note_chords, nb_notes_total, midi_moy, midi_min,
                midi_max, ambitus,min_string,max_string,mean_string,min_fret,
                max_fret,mean_fret,div_rythm, div_notes, div_chords, itv_min,
                itv_max, div_itv, nb_letring,presence_letring, letring100pourcents,
                vibratoW, presence_vibratoMG, whammy, presence_bends,
                presence_fifth, presence_triad]

    return None # empty measure

def freq_to_midi(f):
    """Transforme une fréquence en sa valeur midi"""
    return round(   69+12*log2(f/440)   )

####
####

def get_heat_map(mes):
    heat_map = {}
    notes = list(mes.recurse().getElementsByClass("Note"))
    chords = list(mes.recurse().getElementsByClass("Chord"))
    for c in chords :
        for c_note in c.notes:
            notes.append(c_note)
    for n in notes :
        string = None
        fret = None
        artics = n.articulations
        for articulation in artics :
            if (isinstance(articulation,articulations.StringIndication)):
                string = int(articulation.number)
            if (isinstance(articulation,articulations.FretIndication)):
                fret = int(articulation.number)
        if string is not None and fret is not None:
            if string not in heat_map:
                heat_map[string] = {}
            if fret in heat_map[string]:
                heat_map[string][fret] += 1
            else :
                heat_map[string][fret] = 1
    return heat_map

def heat_map_addition(hm1,hm2):
    global_hm = hm1.copy()
    for key,value in hm2.items():   # pour chaque string de hm2
        if key not in global_hm:
            global_hm[key] = {}
        for key2,value2 in value.items():   # pour chaque frette de cette string de hm2
            if key2 in global_hm[key]:
                global_hm[key][key2]+=value2
            else :
                global_hm[key][key2] = value2
    return global_hm

def get_heat_map_number_of_notes(heatmap):
    number = 0
    for key,value in heatmap.items():
        for key2,value2 in value.items():
            number += value2
    return number

def get_mean_string_and_fret_from_heatmap(heatmap):
    string_count = 0
    fret_count = 0
    note_count = 0
    for key,value in heatmap.items():
        for key2,value2 in value.items():
            string_count += value2 * int(key)
            fret_count += value2 * key2
            note_count += value2
    mean_string = round(string_count/note_count,2)
    mean_fret = round(fret_count/note_count,2)
    return mean_string,mean_fret

def get_string_fret_stats_from_heatmap(heatmap):
    string_values = []
    fret_values = []
    string_count = 0
    fret_count = 0
    note_count = 0
    for key,value in heatmap.items():
        string_values.append(key)
        for key2,value2 in value.items():
            fret_values.append(key2)
            string_count += value2 * int(key)
            fret_count += value2 * key2
            note_count += value2
    min_string = min(string_values)
    max_string = max(string_values)
    mean_string = round(string_count/note_count,2)
    min_fret = min(fret_values)
    max_fret = max(fret_values)
    mean_fret = round(fret_count/note_count,2)
    return min_string,max_string,mean_string,min_fret,max_fret,mean_fret


def get_mean_string_and_fret_from_measure(m):
    measure_heatmap = get_heat_map(m)
    return get_mean_string_and_fret_from_heatmap(measure_heatmap)

def get_string_fret_stats_from_measure(m):
    measure_heatmap = get_heat_map(m)
    return get_string_fret_stats_from_heatmap(measure_heatmap)

def val_midi(mes) :
    """Renvoie les valeurs :
        midi_moy, midi_min, midi_max"""

    freq_min = 20000 #fréquence minimale
    freq_max = 0 #fréquence maximale
    freq_moy = 0

    notes = mes.recurse().getElementsByClass("Note")
    chords = mes.recurse().getElementsByClass("Chord")


    """Notes jouées une seule à la fois"""

    for note in notes :
        notepitch = note.pitch

        freqnote = notepitch.frequency

        freq_moy+=freqnote

        if freqnote > freq_max :
            freq_max = freqnote
        if freqnote < freq_min :
            freq_min = freqnote

    """Notes au sein d'accords"""

    nb_notes_total = len(notes)

    for chord in chords :
        cp = chord.pitches
        for p in cp :

            freqnote = p.frequency

            if freqnote > freq_max :
                freq_max = freqnote
            if freqnote < freq_min :
                freq_min = freqnote

            freq_moy+=p.frequency
            nb_notes_total+=1

    """Calcul des valeurs Midi"""

    freq_moy/=nb_notes_total
    midi_moy = freq_to_midi(freq_moy)
    midi_max = freq_to_midi(freq_max)
    midi_min = freq_to_midi(freq_min)

    return midi_moy, midi_min, midi_max


#####
#####

def comptage_notes(mes):
    """Renvoie le nombre de chaque valeur midi modulo 12
    [  nombre de Do, nombre de Do#, ..., nombre de Si   ]"""
    notes = mes.recurse().getElementsByClass("Note")
    chords = mes.recurse().getElementsByClass("Chord")

    N = [0 for i in range(12)]

    for n in notes :
        midi_modulo12 = int(n.pitch.ps)%12
        N[midi_modulo12]+=1

    for c in chords :
        cp = c.pitches
        for pitch in cp :
            midi_modulo12 = int(pitch.ps)%12
            N[midi_modulo12]+=1

    return N





def diversite_notes_et_chords(mes) :
    """Renvoie les valeurs :
        diversité de notes jouées individuellement
        diversité d'accords
        diversité dans les notes jouées (individuelles et en accord)

        nombre de notes jouées individuellement
        nombre d'accords joués
        nombre de notes total (individuelles et au sein d'un accord"""

    Dnotes = []
    Dchords = []
    Dnotes_total = []

    notes = mes.recurse().getElementsByClass("Note")
    chords = mes.recurse().getElementsByClass("Chord")


    """Notes jouées une seule à la fois"""

    for note in notes :
        notepitch = note.pitch
        if notepitch not in Dnotes :
            Dnotes.append(notepitch)
            Dnotes_total.append(notepitch)

    """Notes au sein d'accords"""

    nb_notes_total = len(notes)

    for chord in chords :
        cp = chord.pitches
        L = []
        for p in cp :
            nb_notes_total += 1
            L.append(p.midi)
        if L not in Dchords :
            Dchords.append(L)
            for p in cp :
                if p not in Dnotes_total :
                    Dnotes_total.append(p)

    return len(Dnotes), len(Dchords), len(Dnotes_total), len(notes), len(chords), nb_notes_total


#####
#####



def diversite_durees(mes) :
    """Renvoie la diversité rythmique d'une mesure (nombre de rythmes différents)"""

    L_rythms = []

    notes = mes.recurse().getElementsByClass("Note")
    chords = mes.recurse().getElementsByClass("Chord")

    for note in notes :
        rythm = note.duration.quarterLength
        if rythm not in L_rythms :
            L_rythms.append(rythm)

    for chord in chords :
        rythm = chord.duration.quarterLength
        if rythm not in L_rythms :
            L_rythms.append(rythm)

    return len(L_rythms)

####
####


def tailles_accords(mes) :
    """Renvoie les valeurs :
        -nombre d' "Accords" à 1 élément (notes)
        -nombre d'Accords à 2 éléments
        - ...
        -nombre d'Accords à 6 éléments"""

    notes = mes.recurse().getElementsByClass("Note")
    chords = mes.recurse().getElementsByClass("Chord")

    L = [len(notes),0,0,0,0,0]

    for c in chords :
        nb_notes = len(c.pitches)
        L[nb_notes-1]+=1

    return L


####
####

def intervalles(mes) :
    """Renvoie les intervalles min et max
    --> Si on a du note à note on regarde simplement l'intervalle entre ces 2 notes
    --> Si on tombe sur un accord, on considère la note la plus aiguë"""
    notes = mes.recurse().getElementsByClass("Note")
    chords = mes.recurse().getElementsByClass("Chord")
    L = []

    """On range toutes les notes et chords dans L en les triant par leurs offsets"""
    for n in notes :
        offset = n.offset
        i = 0
        while i<len(L) and L[i].offset < offset :
            i+=1
        L = L[0:i] + [n] + L[i:len(L)]

    for c in chords :
        offset = c.offset
        i = 0
        while i<len(L) and L[i].offset < offset :
            i+=1
        L = L[0:i] + [c] + L[i:len(L)]

    """On répertorie l'ensemble des intervalles"""
    ancien_midi = None
    itv = None
    Litv = []

    for l in L :
        if isinstance(l,note.Note):
            midi_note = freq_to_midi(l.pitch.frequency)
            if ancien_midi != None :
                itv = abs(midi_note-ancien_midi)

        elif isinstance(l,chord.Chord) :
            """Pour un accord on considère sa note la plus aiguë"""
            midi_note = freq_to_midi(    max([pitch.frequency for pitch in l.pitches])    )
            if ancien_midi != None :
                itv = abs(midi_note-ancien_midi)

        if itv != None and itv not in Litv :
            Litv.append(itv)

        ancien_midi = midi_note

    """Détermination des intervalles max et min"""
    if Litv != [] :
        itv_max = max(Litv)
        itv_min = min(Litv)
    else :
        itv_max, itv_min = 0,0

    return itv_max, itv_min, len(Litv) # on renvoie les intervalles max et min et le nombre d'intervalles distincts

####
####

def bend_et_vibrato(mes):
    """
    Vibrato W = Notation \/\/\/ (avec barre de vibrato)
    Vibrato Main gauche = Notation GuitarPro ~~~
    Whammy = Barre de Vibrato (dive, dip, ...)
    Bend
    """

    vibratoW = 0
    vibratoMG = 0
    bend = 0
    whammy = 0
    notes = mes.recurse().getElementsByClass("Note")
    chords = mes.recurse().getElementsByClass("Chord")
    for note in notes :
        parole = note.lyric
        if parole!= None :
            if '\\/\\/\\/' in parole :
                vibratoW+=1
            if '~~~' in parole :
                vibratoMG+=1
            if '<bend>' in parole :
                bend+=1
            if 'w/bar' in parole :
                whammy+=1

    for chord in chords :
        parole = chord.lyric
        if parole!= None :
            if '\\/\\/\\/' in parole :
                vibratoW+=1
            if '~~~' in parole :
                vibratoMG+=1
            if '<bend>' in parole :
                bend+=1
            if 'w/bar' in parole :
                whammy+=1

    return vibratoW, vibratoMG, whammy, bend

def presence_fifth_and_triad(mes):
    # return 2 booleans : presence_fifth, presence_triade
    notes = list(mes.recurse().getElementsByClass("Note"))
    chords = list(mes.recurse().getElementsByClass("Chord"))
    presence_fifth = False
    for c in chords :
        for c_note in c.notes:
            notes.append(c_note)
    pc_count = [0 for i in range(12)]
    for n in notes :
        pc = int(n.pitch.ps)%12
        pc_count[pc]=1
    for i in range(len(pc_count)):
        if pc_count[i] == 1:
            if pc_count[(i+7)%12] == 1:
                presence_fifth = True
                if pc_count[(i+3)%12] == 1 or pc_count[(i+4)%12] == 1:
                    return True,True

    return presence_fifth,False

def detection_triade(mes):
    """Renvoie la présence ou non d'une triade majeure/mineure dans la mesure
    en se basant sur les notes les plus fréquentes"""

    notes = mes.recurse().getElementsByClass("Note")
    chords = mes.recurse().getElementsByClass("Chord")

    N = [0 for i in range(12)]

    for note in notes :
        midi_modulo12 = freq_to_midi(note.pitch.frequency)%12
        N[midi_modulo12]+=1

    for chord in chords :
        cp = chord.pitches
        for pitch in cp :
            midi_modulo12 = freq_to_midi(pitch.frequency)%12
            N[midi_modulo12]+=1

    S = [n for n in N]
    S.sort()

    triademin = 0
    triademaj = 0
    powerchord = 0

    #Liste des valeurs midi modulo 12 les plus fréquentes
    L = [N.index(S[-1])  ,  N.index(S[-2])  ,  N.index(S[-3])  ,  N.index(S[-4])]
    for fond in L : #On considère chaque élément de L comme potentielle fondamentale
        quinte = (fond+7)%12
        if quinte in L : #Si on a tonique quinte, on a au moins un powerchord
            powerchord = 1
            tiercemin = (fond+3)%12
            tiercemaj = (fond+4)%12

            if tiercemin in L :
                triademin = 1
                powerchord = 0 #On n'a plus powerchord si une 3rce est présente
            if tiercemaj in L :
                triademaj = 1
                powerchord = 0

    return powerchord, triademaj, triademin


def time_sig(mes, lasttimesig) :
    """Renvoie la signature rythmique de la mesure 4/4 , 7/8, ...
    Si il n'y en a pas de nouvelle, il faut renvoyer l'ancienne"""

    timesign = mes.timeSignature
    if timesign != None :
        lasttimesign = timesign
    #if str(lasttimesign) == str(meter.TimeSignature('4/4')) :
    return lasttimesign


####
####

def durees_plus_longues_plus_courtes(mes) :
    """Renvoie durée note la plus courte / durée mesure
               durée note la plus longue / durée mesure

               proportion de silence dans la mesure"""

    notes = mes.recurse().getElementsByClass("Note")
    chords = mes.recurse().getElementsByClass("Chord")
    rests = mes.recurse().getElementsByClass("Rest")

    durmax = 0
    durmin = 100
    durtot = 0

    dursilence = 0

    for note in notes :
        dur = note.duration.quarterLength
        durtot+=dur
        if dur > durmax :
            durmax = dur
        if dur < durmin :
            durmin = dur

    for chord in chords :
        dur = chord.duration.quarterLength
        if dur > durmax :
            durmax = dur
        if dur < durmin :
            durmin = dur

    for rest in rests :
        dur = rest.duration.quarterLength
        durtot+=dur
        dursilence+=dur

    return float(durmin/durtot), float(durmax/durtot), float(dursilence/durtot)

#####
#####


def solo_ou_acc(mes) :
    """Renvoie la fonction de la mesure si elle comporte une indication
    fun:acc ou fun:solo ... Sinon il faudra prendre la dernière annotation rencontrée"""

    notes = mes.recurse().getElementsByClass("Note")
    chords = mes.recurse().getElementsByClass("Chord")
    rests = mes.recurse().getElementsByClass("Rest")

    for note in notes :
        parole = note.lyric
        if parole!= None :
            if 'fun:acc' in parole :
                return 'fun:acc'
            elif 'fun:solo' in parole :
                return 'fun:solo'


    for rest in rests :
        parole = rest.lyric
        if parole!= None :
            if 'fun:acc' in parole :
                return 'fun:acc'
            elif 'fun:solo' in parole :
                return 'fun:solo'

    for chord in chords :
        parole = chord.lyric
        if parole!= None :
            if 'fun:acc' in parole :
                return 'fun:acc'
            elif 'fun:solo' in parole :
                return 'fun:solo'

    return 0 #Si on renvoie 0, c'est qu'il n'y a pas eu d'indication dans la mesure


#####
#####


def letring(mes) :
    """Renvoie le nombre de notes 'letring' dans la mesure (0 s'il n'y en a pas)"""
    cpt_lr = 0

    notes = mes.recurse().getElementsByClass("Note")
    chords = mes.recurse().getElementsByClass("Chord")

    for note in notes :
        parole = note.lyric
        if parole!= None :
            if 'LetRing' in parole :
                cpt_lr += 1

    for chord in chords :
        parole = chord.lyric
        if parole!= None :
            if 'LetRing' in parole :
                cpt_lr += 1

    return cpt_lr

####
####
