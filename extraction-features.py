#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from music21 import *
from parserGP import ParserGP
from fractions import Fraction
from math import log2, log
import os
import csv

from utils_extraction_features import *
from utils_training import build_and_get_bidirectional_model


def extract_features(with_labels):
    """creates a feature csv file"""

    tableau_csv = [get_features_table_headers(with_labels=with_labels)]

    mes_solo = 0 #Vaut 0 si le dernier label rencontré est 'fun:acc' 1 si le dernier rencontré est fun:solo

    nsolo = 0   #Nb mesures solo
    nacc = 0    #Nb mesures acc
    nvide = 0   #Nb mesures vides
    nmalnotees = 0 #Mesures mal annotées

    rhythm_guitar_heatmap = {}
    other_heatmap = {}

    gpif_dir = './data'
    for file in sorted(os.listdir(gpif_dir)):
        if os.path.isfile(gpif_dir+'/'+file) and file.endswith('.gp'):
            score_gpif = gpif_dir+'/'+file.replace('.gp','')+'/score.gpif'
            s = ParserGP().parseFile(score_gpif)
            print("extract features from {}".format(score_gpif))
            verif_annot = 0 # to check that there is an annotation on the first bar of the file
            title = s.metadata.title
            artist = s.metadata.getContributorsByRole('artist')[0].name
            titre = title+'\n'+artist
            parties = s.parts
            lp = len(parties)
            for i in range(lp) :
                partie = parties[i]
                nompartie = partie.partName

                num_mes = 0
                for mes in partie :
                    num_mes+=1
                    fonction = solo_ou_acc(mes)
                    if fonction != 0:
                        mes_solo = int(fonction=='fun:solo')

                    measure_features = extract_features_from_measure(mes)

                    if measure_features is not None : # non empty measure

                        tableau_csv.append([str(score_gpif), titre, nompartie, num_mes, mes_solo, 1-mes_solo]+measure_features)
                        measure_heatmap = get_heat_map(mes)

                        if mes_solo == 1 :
                            nsolo+= 1
                            other_heatmap = heat_map_addition(other_heatmap,measure_heatmap)
                        else :
                            nacc+= 1
                            rhythm_guitar_heatmap = heat_map_addition(rhythm_guitar_heatmap,measure_heatmap)

                    else : # empty measure
                        nvide+=1

    # writing the csv file
    with open("./function_guitar.csv", "w") as f_write:
        writer = csv.writer(f_write)
        for row in tableau_csv:
            writer.writerow(row)

    print(   'nb_MesuresSolo = '+str(nsolo) +' // nb_MesuresAcc = '+str(nacc) +' // nb_MesuresVides = '+str(nvide) +' // nb_Mesures_Mal_Annotees = '+str(nmalnotees)   )
    print('rhythm_guitar_heatmap : {}'.format(rhythm_guitar_heatmap))
    print('other_heatmap : {}'.format(other_heatmap))

extract_features(with_labels = True)
