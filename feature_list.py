'''
To easily print the list of computed features from function_guitar.csv:
    import pandas as pd
    df = pd.read_table("function_guitar.csv",sep=',')
    f_list = list(df.columns)[6:]
    d = {}
    for f in f_list:
        d[f]=True
    d
'''


used_features = {
 # note features
'Has_SingleNotes': True,
 'Nb_Notes_Total': True,
 'Midi_moy': True,
 'Midi_min': True,
 'Midi_max': True,
 'Ambitus': True,
 'Div_durees': True,
 'Div_notes': True,
 'Intervalle_min': True,
 'Intervalle_max': True,
 'Div_intervalles': True,

 # chord features
 'Has_Chords': True,
 'Has_2_note_chords': True,
 'Has_3_note_chords': True,
 'Has_4_note_chords': True,
 'Has_5_note_chords': True,
 'Has_6_note_chords': True,
 'Div_chords': True,
  'has_triad': True,
  'has_fifth': True,

 # tab features
'Min_string':True,
'Max_string':True,
'String_moy':True,
'Min_fret':True,
'Max_fret':True,
'Fret_moy':True,
 'Nb_Letring': True,
 'Letring_Present': True,
 'Letring100pourcents': True,
 'Nb_VibratoW': True,
 'Presence_VibratoMG': True,
 'Nb_Whammy_Bar': True,
 'Presence_bends': True
}

active_features_list = [k for k,v in used_features.items() if v]