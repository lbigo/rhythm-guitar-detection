#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from numpy.lib.function_base import blackman
from feature_list import *
import os,sys
from pathlib import Path
import numpy as np
from numpy.random import seed
seed(2)

import tensorflow as tf
tf.random.set_seed(2)

from keras.models import Sequential
from keras.models import Model
from keras import layers
from keras import Input
from keras.layers import Dense, SimpleRNN, LSTM
from keras.models import model_from_yaml
from keras import backend as K
from keras.models import load_model
from sklearn.metrics import confusion_matrix #, classification_report
import csv
import pandas as pd
pd.options.display.max_rows = 10

from utils_training import *

bidirectional_training_bool = True

df = pd.read_table("./function_guitar.csv",sep = ',',header = 0)

predicted_labels_file_path = './tmp-predicted-annotations'
reference_labels_file_path = './tmp-manual-annotations'

MDFeatures = min_delta(df,active_features_list)

song_name_list = get_song_name_list(df)
number_of_bars_in_corpus = df.shape[0]

def split_data_par_morceaux(i):
    """Données apprentissage Appr de la forme :
        [
                [   V1, V2, V3   ] Les 0 sont des vecteurs nuls de features (tous à 0) --> Zero Padding
                [   V1, V2, V3, V4, V5  ]
                [   V1, V2   ]
        ]

        """

    test_song_name = song_name_list[i]

    training_file_names, validation_file_names = get_train_val_file_names(df,test_song_name)

    training_feature_values = [[] for i in range(len(training_file_names))] # one empty list for each training gpif file
    training_label_values = [[] for i in range(len(training_file_names))]
    training_bar_numbers = [[] for i in range(len(training_file_names))]

    validation_feature_values = [[] for i in range(len(validation_file_names))]
    validation_label_values = [[] for i in range(len(validation_file_names))]
    validation_bar_numbers = [[] for i in range(len(validation_file_names))]

    number_of_bars_in_corpus = df.shape[0]

    for bar_n in range(number_of_bars_in_corpus) :

        file_name = df.at[bar_n,'Fichier']
        V = vecteur(bar_n,df,MDFeatures,active_features_list,label=True)  # [line_number,manual_label,normalize_feat_1,...]

        if file_name in training_file_names :
            training_feature_values[training_file_names.index(file_name)].append(V[2:]) # For each training file (length of the list) : a list of bars represented as feature vectors
            training_label_values[training_file_names.index(file_name)].append(V[1])
            training_bar_numbers[training_file_names.index(file_name)].append(bar_n)

        else :
            validation_feature_values[validation_file_names.index(file_name)].append(V[2:])
            validation_label_values[validation_file_names.index(file_name)].append(V[1])
            validation_bar_numbers[validation_file_names.index(file_name)].append(bar_n)

    return training_feature_values,validation_feature_values, training_label_values, validation_label_values, training_bar_numbers, validation_bar_numbers



"""Evaluating the model on song at index i"""
def evaluation(i) :
    recurrence_bar_number = 5
    predicted_labels = None
    training_feature_values,validation_feature_values, training_label_values, validation_label_values, training_bar_numbers, validation_bar_numbers = split_data_par_morceaux(i)
    forward_validation_feature_values = build_cons_forward_bars_groups(validation_feature_values,validation_bar_numbers, n=recurrence_bar_number)
    flat_validation_label_values = [item for sublist in validation_label_values for item in sublist]
    # LV = [item for sublist in validation_bar_numbers for item in sublist]
    if bidirectional_training_bool:
        model = build_and_get_bidirectional_model(training_feature_values, training_label_values, training_bar_numbers,recurrence_bar_number)
        # model = load_model('modele.h5')
        """Prédictions sur chaque élément de data_test puis matrice de confusion"""
        backward_validation_feature_values = build_cons_backward_bars_groups(validation_feature_values,validation_bar_numbers, n=recurrence_bar_number)
        predicted_labels = model.predict({'forward_groups':forward_validation_feature_values,'backward_groups':backward_validation_feature_values})
    else :
        model = build_and_get_unidirectional_model(training_feature_values,training_label_values,training_bar_numbers,recurrence_bar_number)
        # model = load_model('modele.h5')
        """Prédictions sur chaque élément de data_test puis matrice de confusion"""
        predicted_labels = model.predict(np.array(forward_validation_feature_values))

    predicted_labels = [p[0] for p in predicted_labels]
    return predicted_labels, flat_validation_label_values

def main_val_predictions():

    print("features:{}".format(used_features))
    predicted_labels_tab = []
    reference_labels_tab = []

    for i in range(len(song_name_list)) : # Leave-one-song-out evaluation

        print(str(i+1)+'/'+str(len(song_name_list)))

        song_name = song_name_list[i]
        predicted_labels, reference_labels = evaluation(i)
        predicted_labels_tab.append([song_name]+predicted_labels)
        reference_labels_tab.append([song_name]+reference_labels)

    with open(predicted_labels_file_path, "w") as f_write:
        writer = csv.writer(f_write)
        for row in predicted_labels_tab :
            writer.writerow(row)

    with open(reference_labels_file_path, "w") as f_write:
        writer = csv.writer(f_write)
        for row in reference_labels_tab :
            writer.writerow(row)

def build_results_df(feature_list):
    preds_reader = csv.reader(open(predicted_labels_file_path, "r", newline="",encoding='utf8'))
    refs_reader = csv.reader(open(reference_labels_file_path, "r", newline="",encoding='utf8'))

    pred_labels_per_song = list(preds_reader)
    ref_labels_per_song = list(refs_reader)

    song_number = len(pred_labels_per_song)
    eval_measure_list = []

    for song_index in range(song_number):
        song_pred_labels = pred_labels_per_song[song_index]
        song_ref_labels = ref_labels_per_song[song_index]
        assert len(song_pred_labels)==len(song_ref_labels),'song_ref_labels and song_pred_labels are not the same size'
        song_title = song_pred_labels[0]
        assert song_pred_labels[0] == song_ref_labels[0],'song not the same names {} {}'.format(song_pred_labels[0],song_ref_labels[0])
        song_pred_labels = song_pred_labels[1:]
        song_ref_labels = song_ref_labels[1:]
        song_df = df.loc[df['Track_Name'] == song_title]
        assert song_df.shape[0] == len(song_pred_labels), "error, should be the same size {} {} - guitar_function.csv and csv results are not consistent".format(song_df.shape[0],len(ligne[1:]))
        in_song_index = 0
        for index, row in song_df.iterrows():
            song_df.iloc[in_song_index]
            ref_label = song_ref_labels[in_song_index]
            pred_label = round(float(song_pred_labels[in_song_index]),2)
            measure_line = [row['Fichier'],row['Track_Name'],row['Partie'],row['Mesure'],ref_label,pred_label]
            for feature in feature_list:
                measure_line.append(row[feature])
            # print('song {} file {} part {} mes {} : ref = {} ; pred = {}'.format(row['Track_Name'],row['Fichier'],row['Partie'],row['Mesure'],ref_label,pred_label))
            eval_measure_list.append(measure_line)
            in_song_index+=1


    output_df_columns = ['Fichier','Track_Name','Partie','Mesure','Ref','Pred']
    for feature in feature_list:
        output_df_columns.append(feature)
    output_df = pd.DataFrame(np.array(eval_measure_list),columns=output_df_columns)
    output_df.to_csv('./results.csv')

main_val_predictions()
build_results_df(used_features)
