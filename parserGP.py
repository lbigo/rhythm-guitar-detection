import xml.etree.ElementTree as ET
from music21 import *

class ParserGP(converter.subConverters.SubConverter):

    registerFormats = ('gpif', )
    registerInputExtensions = ('gpif', )

    def parseFile(self, XMLFileName, number=None):
        # root is the GPIF node at the top of the gpif file
        self.root = ET.parse(XMLFileName).getroot()
        self.__initScore()
        return self.stream

    # Create a Stream structure (Score > Part > Measure > Voice > Notes)
    def __initScore(self):
        # Set Score metadatas
        titre = 'No title'
        sous_titre = ''
        artiste = 'No artist'
        tag_titre = self.root.find("./Score/Title")
        tag_sous_titre = self.root.find("./Score/SubTitle")
        tag_artiste = self.root.find("./Score/Artist")
        if tag_titre is not None:
            titre = tag_titre.text
        if tag_sous_titre is not None:
            sous_titre = tag_sous_titre.text
        if tag_artiste is not None:
            artiste = str(tag_artiste.text)

        # nom_complet = titre+'\n'+artiste+'\n'+sous_titre
        # meta = metadata.Metadata(title=nom_complet)

        meta = metadata.Metadata()
        meta.title = titre
        meta.alternativeTitle = sous_titre

        artist_contributor = metadata.Contributor()
        artist_contributor.name=artiste
        artist_contributor.role='artist'
        meta.addContributor(artist_contributor)

        self.stream.metadata = meta        
        # Create Part according to guitar tracks in standard tuning
        for track in self.root.findall("./Tracks/Track"):
            # Select track according to icon (guitar icons) and tunning (standard tuning) and track.find(".//*[@name='Tuning']/Pitches").text == "40 45 50 55 59 64"
            if track.find("./IconId").text in ["1","2","4","5","23","24","25","26","22"]:
                p = stream.Part()
                p.partName = track.find("Name").text
                p.id = track.get("id")
                self.stream.append(p)
            else:
                print("Unfitted track (tuned differently or non guitar icon) : " + track.find("Name").text)

        partTab = []
        # Append Measures into a temp tab whitch will be append later in parts
        for part in self.stream.parts:
            mtab = []
            tmpTime = ""
            
            """"""
            #
            prev_key_sig = 0
            #
            """"""
            
            for masterBar in self.root.findall("./MasterBars/MasterBar"):
                time = masterBar.find("./Time").text
                bar = masterBar.find("./Bars")
                
                """Adding sharps and flats"""
                """"""
                #
                armure = masterBar.find("./Key/AccidentalCount").text # --> armure est un str : '0' , '-1' , '2'
                key_sig = key.KeySignature(int(armure))
                #
                """"""
                
                m = stream.Measure()
                if time != tmpTime:
                    m.timeSignature = meter.TimeSignature(time)
                
                """"""
                #
                if key_sig != prev_key_sig :    
                    m.keySignature = key_sig
                
                key_sig = prev_key_sig
                #
                """"""
                
                tmpTime = time
                m.id = bar.text.split()[int(part.id)]
                mtab.append(m)
            partTab.append(mtab)

        # Append Voices into previously created measure
        for i in range(len(partTab)):
            for j in range(len(partTab[i])):
                simileMark = self.root.find("./Bars/Bar[@id='" + partTab[i][j].id + "']/SimileMark")
                if simileMark is not None:
                    # if there is a simile mark we go back to the previous measure element
                    if simileMark.text == "Simple":
                        for index, voice in enumerate(partTab[i][j-1].voices):
                            v = stream.Voice()
                            v.id = index+1
                            v.idGpif = voice.idGpif
                            partTab[i][j].append(v)
                    else:
                        # for double simile mark we go back 2 measure before
                        for index, voice in enumerate(partTab[i][j-2].voices):
                            v = stream.Voice()
                            v.id = index+1
                            v.idGpif = voice.idGpif
                            partTab[i][j].append(v)
                else:
                    # When there is no simile mark we fetch voices in XML file
                    for voices in self.root.findall("./Bars/Bar[@id='" + partTab[i][j].id + "']/Voices"):
                        for index, idVoice in enumerate(voices.text.split()):
                            # Voice is a list of 4 ids where -1 means there is no voice
                            if idVoice != '-1':
                                v = stream.Voice()
                                v.id = index+1
                                v.idGpif = idVoice
                                partTab[i][j].append(v)
        self.__addNotestoScore(partTab)
        for i, p in enumerate(partTab):
            for m in p:
                self.stream.parts[i].append(m)


    # Add all notes from xml in the structure previously created
    def __addNotestoScore(self, partTab):
        
        """ indicates if the LetRing annotation has been activated on the whole track in GP"""
        letring_throughout = self.root.find("./Tracks/Track[@id='0']/LetRingThroughout")
        
        for i in range(len(partTab)):
            for j in range(len(partTab[i])):
                for voice in partTab[i][j-1].voices:
                    offset = 0.0
                    beats = self.root.findall("./Voices/Voice[@id='" + str(voice.idGpif) + "']/Beats")
                    if len(beats):
                        # Browse all beats in the voice
                        for idBeat in beats[0].text.split():
                            notesEl = self.root.findall("./Beats/Beat[@id='" + idBeat + "']/Notes")
                            cXML = self.root.findall("./Beats/Beat[@id='" + idBeat + "']/Chord")
                            
                            """"""
                            #
                            legato = self.root.find("./Beats/Beat[@id='" + idBeat + "']/Legato")
                            if legato!= None :
                                val_origin = legato.get('origin')               #val_origin/desination : str -> 'true' or 'false'
                                val_destination = legato.get('destination')
                                
                                
                            fun = self.root.find("./Beats/Beat[@id='" + idBeat + "']/FreeText")
                            
                            VibratoWTremBar = self.root.find("./Beats/Beat[@id='" + idBeat + "']/Properties/Property[@name='VibratoWTremBar']")
                            Whammy = self.root.find("./Beats/Beat[@id='" + idBeat + "']/Whammy")
                            
                            
                            chordName = None
                            graceXML = self.root.findall("./Beats/Beat[@id='" + idBeat + "']/GraceNotes")
                            c = None
                            if len(cXML): # Managing Lyrics (for chord symbols)
                                idChord = cXML[0].text
                                chordName = self.root.find("./Tracks/Track/Staves/Staff/Properties/Property[@name='DiagramCollection']/Items/Item[@id='" + idChord + "']").get("name")
                                
                            if len(notesEl):
                                idNotes = notesEl[0].text.split()
                                notes = []
                                bend = None
                                lr = None
                                vibwide = None
                                for idNote in idNotes:
                                                                                                            
                                    """ parsing different annotations """
                                    if lr == None:
                                        lr = self.root.find("./Notes/Note[@id='" + idNote + "']/LetRing")
                                    if vibwide == None:
                                        vibwide = self.root.find("./Notes/Note[@id='" + idNote + "']/Vibrato")
                    
                                    if bend == None :
                                        bend = self.root.find("./Notes/Note[@id='" + idNote + "']/Properties/Property[@name='Bended']/Enable")
                                    
                                    
                                    Notelr = self.__getNoteFromId(idBeat, idNote)
                                    """String number in lyrics"""
                                    Notelr.addLyric('ST'+str(self.root.find("./Notes/Note[@id='" + idNote + "']/Properties/Property[@name='String']/String").text))
                                    


                                    if legato!= None :
                                        if val_origin == 'true' :
                                            if val_destination == 'false' :
                                                Notelr.addLyric('\___')
                                            else :
                                                Notelr.addLyric('____')
                                        else :
                                            Notelr.addLyric('___/')
                                            
                                    if lr != None :
                                        Notelr.addLyric('LetRing')
                                    
                                    
                                    if letring_throughout != None :
                                        Notelr.addLyric('LetRing')
                                        
                                    
                                    if fun!= None :
                                        Notelr.addLyric(fun.text)
                                        
                                    if VibratoWTremBar != None :
                                        Notelr.addLyric('\\/\\/\\/')
                                        
                                    if Whammy != None :
                                        Notelr.addLyric('w/bar')
                                        
                                    if bend != None :
                                        Notelr.addLyric('<bend>')
                                        
                                    if vibwide != None :
                                        Notelr.addLyric('~~~')
                                        
                                    #
                                    """"""
                                    
                                    notes.append(Notelr)
                                    
                                if len(notes) > 1:                                    
                                    
                                    notes = chord.Chord(notes)
                                    
                                    """Adding LETRING and FUN:ACC / FUN:SOLO for chords"""
                                    #
                                    if lr!= None :
                                        notes.addLyric('LetRing')
                                    
                                    if letring_throughout != None :
                                        notes.addLyric('LetRing')
                                    
                                    if fun!= None :
                                        notes.addLyric(fun.text)
                                        
                                    if VibratoWTremBar != None :
                                        notes.addLyric('\\/\\/\\/')
                                        
                                    if bend != None :
                                        notes.addLyric('<bend>')
                                        
                                    if vibwide != None :
                                        notes.addLyric('~~~')
                                    
                                    if Whammy != None :
                                        notes.addLyric('w/bar')
                                    
                                    #
                                    """"""
                                
                            
                                    
                                if chordName:
                                    if isinstance(notes, chord.Chord):
                                        notes.addLyric(chordName)              #Chord symbol as Lyrics
                                    else:
                                        notes[0].addLyric(chordName)                                     
                                
                                
                                if len(graceXML):
                                    notes[0].duration = duration.Duration("eighth")
                                    graceNote = notes[0].getGrace()
                                    if graceXML[0].text == "OnBeat":
                                        graceNote.duration.slash = False
                                    voice.insert(offset, graceNote)
                                else:
                                    voice.insert(offset, notes if isinstance(notes, chord.Chord) else notes[0])
                                    offset += notes[0].duration.quarterLength
                            else:
                                r = note.Rest()
                                r.duration = self.__getRythmDurationFromIdBeat(idBeat)
                                if c:
                                    r.addLyric(chordName)
                                 
                                    
                                """"""    
                                #
                                if fun!= None :
                                    r.addLyric(fun.text)
                                #
                                """"""
                                
                                voice.insert(offset, r)
                                offset += r.duration.quarterLength

    # Get Note of a specified beat (for rythm) from xml according to a given id
    def __getNoteFromId(self, idBeat, idNote):
        midiPitch = self.root.find("./Notes/Note[@id='" + idNote + "']/Properties/Property[@name='Midi']/Number").text
        n = note.Note(int(midiPitch))

        # Ignore bend and transpose original note according to the destination value of the bend
        bendValue = self.root.findall("./Notes/Note[@id='" + idNote + "']/Properties/Property[@name='BendDestinationValue']/Float")
        additionalSemiTone = 0
        if bendValue != []:
            #TODO add a warning for bends < semitone
            additionalSemiTone = int(float(bendValue[0].text)/50)
            bendInterval = interval.Interval(additionalSemiTone)
            n = n.transpose(bendInterval)

        if self.root.findall("./Notes/Note[@id='" + idNote + "']/Tie[@origin='true']") != []:
            n.tie = tie.Tie('start')

        if self.root.findall("./Notes/Note[@id='" + idNote + "']/Tie[@destination='true']") != []:
            n.tie = tie.Tie('stop')


        # Add String as an Articulation
        string = articulations.StringIndication()
        string.number = self.root.find("./Notes/Note[@id='" + idNote + "']/Properties/Property[@name='String']/String").text

        # Add Fret as an Articulation
        fret = articulations.FretIndication()
        fret.number = str(int(self.root.find("./Notes/Note[@id='" + idNote + "']/Properties/Property[@name='Fret']/Fret").text) + additionalSemiTone)

        # Add articulations to note
        n.articulations = [string, fret]

        # Rythm
        n.duration = self.__getRythmDurationFromIdBeat(idBeat)

        return n

    # Get Duration type of the rhythm of a specified beat
    def __getRythmDurationFromIdBeat(self, idBeat):
        d = duration.Duration()
        rhythm = self.root.findall("./Beats/Beat[@id='" + idBeat + "']/Rhythm")
        if len(rhythm):
            idRhythm = rhythm[0].get("ref")
            rhythm = self.root.findall("./Rhythms/Rhythm[@id='" + idRhythm + "']")
            if len(rhythm):
                noteValue = rhythm[0].find("NoteValue").text.lower()
                dots = 0
                if rhythm[0].find("AugmentationDot") is not None:
                    dots = rhythm[0].find("AugmentationDot").get("count")
                d.quarterLength = duration.convertTypeToQuarterLength(noteValue, float(dots))
                if rhythm[0].find("PrimaryTuplet") is not None:
                    num = rhythm[0].find("PrimaryTuplet").get("num")
                    den = rhythm[0].find("PrimaryTuplet").get("den")
                    d.appendTuplet(duration.Tuplet(int(num), int(den)))
        return d

    def __getPitchFromXML(self, XMLPitch):
        stringPitch = XMLPitch.find("Step").text
        stringPitch += XMLPitch.find("Accidental").text.replace("b", "-") if XMLPitch.find("Accidental").text is not None else ""
        stringPitch += XMLPitch.find("Octave").text
        return stringPitch

    # Write a stream in a skeleton gp file
    def writeGPFromStream(self, stream):

        return
