import pandas as pd
import numpy as np
pd.options.display.max_rows = 10

import tensorflow as tf
tf.random.set_seed(2)

from keras.models import Sequential
from keras.models import Model
from keras import layers
from keras import Input
from keras.layers import Dense, SimpleRNN, LSTM
from keras.models import model_from_yaml
from keras import backend as K
from keras.models import load_model
from sklearn.metrics import confusion_matrix #, classification_report


def min_delta(df,list_features):
    """ returns a list of the form
        [   [minfeature1, deltafeature1],[minfeature2,deltafeature2],[...]   ]
        computed from dataframe df
    """
    min_delta_features = []

    for feat in list_features :
        maxfeat = df[feat].max()
        minfeat = df[feat].min()
        deltafeat = maxfeat-minfeat
        min_delta_features.append([minfeat,deltafeat])

    return min_delta_features

def get_song_name_list(df) :
    """Returns the list of songs (multiple tracks can be associated with one same song)"""
    titres = df['Track_Name']
    song_name_list = []
    for titre in titres :
        if titre not in song_name_list :
            song_name_list.append(titre)
    return song_name_list

def get_all_file_names(df): #used to train a model on the whole annotated dataset (for prediction on an exterior dataset, not for LOPO validation)
    training_file_names = []
    number_of_bars_in_corpus = df.shape[0]
    for l in range(number_of_bars_in_corpus) :
        track_name = df.at[l,'Track_Name']
        file_name = df.at[l,'Fichier']
        if file_name not in training_file_names:
            training_file_names.append(file_name)

    return training_file_names


def get_train_val_file_names(df,song_out_name) :
    """Returns training/validation file name lists"""

    training_file_names = []
    validation_file_names = []
    number_of_bars_in_corpus = df.shape[0]
    for l in range(number_of_bars_in_corpus) :
        track_name = df.at[l,'Track_Name']
        file_name = df.at[l,'Fichier']
        if track_name!=song_out_name and file_name not in training_file_names:
            training_file_names.append(file_name)
        elif track_name == song_out_name and file_name not in validation_file_names :
            validation_file_names.append(file_name)

    return training_file_names, validation_file_names

def vecteur(l,df,MDFeatures,active_features_list,label) :

    """Renvoie la ligne l vectorisée"""
    """Etiquette vaut True ou False, si elle vaut True,
    c'est qu'on connaît le rôle de la mesure (corpus annoté), on le met provisoirement dans le vecteur
    Sinon, on n'a aucune connaissance du rôle, on ne peut pas récupérer d'étiquette(MSB)."""
    V = [l]
    if label :
        is_not_rhythm_guitar = df.at[l,'Solo']
        V.append(is_not_rhythm_guitar)
    for k in range(len(active_features_list)) :
        feat = active_features_list[k]
        mini = MDFeatures[k][0]
        delta = MDFeatures[k][1]
        V.append(   ( df.at[l,feat] - mini ) / delta   ) # [line_number,manual_label,normalize_feat_1,...]

    return V

def build_cons_forward_bars_groups(feature_values,bar_numbers,n=3):
    '''Groups (in forward direction) consecutive bars by groups of n'''
    
    L = []
    for l in bar_numbers:
        l = [l[0] for i in range(n-1)] + l
        for i in range(len(l)-n+1):
            L.append( [ l[i+j] for j in range(n)] )

    M = []
    for l in feature_values: # = for each file
        l = [l[0] for i in range(n-1)] + l # duplicate n-1 times the first bar at the beginning of the sequence l
        for i in range(len(l)-n+1): # for each element of l (stopping n before the end) ...
            M.append( [ l[i+j] for j in range(n)] ) # ... build a list of the bar + the n following bars (and append it to M)

    # manage n-batches spanning over disconnected sections
    for ind_frag in range(len(M)) :  # for each n-group
        for ind_tuple in range(n-1) :
            if L[ind_frag][ind_tuple + 1] > L[ind_frag][ind_tuple] + 1 :     # si dans un paquet il y a un "saut de mesures"
                valmes = M[ind_frag][ind_tuple + 1]
                for k in range(ind_tuple + 1) :
                    M[ind_frag][k] = valmes

    return np.array(M)

def build_cons_backward_bars_groups(feature_values,bar_numbers,n=3):
    '''Groups (in backward direction) consecutive bars by groups of n'''
    
    L = []
    
    for l in bar_numbers:
        l_rev = list(reversed(l))
        l_rev = [l_rev[0] for i in range(n-1)] + l_rev
        for i in range(len(l_rev)-n,-1,-1): # ordering groups in increasing order (althought bars in a group are in reverse order)
            L.append( [ l_rev[i+j] for j in range(n)] )

    M = []
    for l in feature_values: # = for each file
        l_rev = list(reversed(l)) # l = [bar0, bar1, ..., barn] - l_rev = [barn, barn-1, ... bar0]
        l_rev = [l_rev[0] for i in range(n-1)] + l_rev # duplicate n-1 times the first element (= the last bar, as the order has been inverted) at the beginning of the sequence l_rev)
        for i in range(len(l_rev)-n,-1,-1): # ordering groups in increasing order (althought bars in a group are in reverse order)
            M.append( [ l_rev[i+j] for j in range(n)] ) # ... build a list of the bar + the n following bars (and append it to M)
    
    # manage n-batches spanning over disconnected sections
    for ind_frag in range(len(M)) : # for each n-group
        for ind_tuple in range(n-1) :
            if L[ind_frag][ind_tuple + 1] < L[ind_frag][ind_tuple] - 1 :    # si dans un paquet il y a un "saut de mesures"
                valmes = M[ind_frag][ind_tuple + 1]
                for k in range(ind_tuple + 1) :
                    M[ind_frag][k] = valmes

    return np.array(M)

def build_and_get_bidirectional_model(training_feature_values, training_label_values, training_bar_numbers,recurrence_bar_number) :

    feature_dim = len(training_feature_values[0][0])

    forward_input = Input(shape=(recurrence_bar_number,feature_dim),name='forward_groups')
    forward_x = layers.LSTM(200)(forward_input)

    backward_input = Input(shape=(recurrence_bar_number,feature_dim),name='backward_groups')
    backward_x = layers.LSTM(200)(backward_input)

    concatenated = layers.concatenate([forward_x,backward_x],axis=-1)
    mixed_x = layers.Dense(75, activation='relu')(concatenated)
    mixed_x = layers.Dense(10, activation='relu')(mixed_x)
    output_label = layers.Dense(1, activation='sigmoid')(mixed_x)

    model = Model([forward_input,backward_input],output_label)

    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

    iter = 12

    forward_training_feature_values = build_cons_forward_bars_groups(training_feature_values,training_bar_numbers,n=recurrence_bar_number)
    backward_training_feature_values = build_cons_backward_bars_groups(training_feature_values,training_bar_numbers,n=recurrence_bar_number)
    flat_training_label_values = [item for sublist in training_label_values for item in sublist]

    model.fit({'forward_groups':forward_training_feature_values,'backward_groups':backward_training_feature_values},np.array(flat_training_label_values), epochs= iter, batch_size = 32)
    """Sauvegarde du modèle"""
    model.save('modele.h5')

    return model

def build_and_get_unidirectional_model(training_feature_values,training_label_values,training_bar_numbers,recurrence_bar_number):
    feature_dim = len(training_feature_values[0][0])

    model = Sequential()
    #model.add(SimpleRNN(units=200, input_dim=(t2), activation="relu"))
    model.add(LSTM(units=200, input_dim=(feature_dim), activation="relu"))
    model.add(Dense(75, activation='relu'))
    model.add(Dense(10, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

    iter = 12

    forward_training_feature_values = build_cons_forward_bars_groups(training_feature_values,training_bar_numbers,n=recurrence_bar_number)
    flat_training_label_values = [item for sublist in training_label_values for item in sublist]

    model.fit(np.array(forward_training_feature_values), np.array(flat_training_label_values), epochs= iter, batch_size = 32)
    """Sauvegarde du modèle"""
    model.save('modele.h5')
    return model