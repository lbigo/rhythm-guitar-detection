import csv
import pandas as pd
import numpy as np
from statistics import mean
import os.path
from parserGP import *

predicted_labels_file_path = './tmp-predicted-annotations'
reference_labels_file_path = './tmp-manual-annotations'
rhythm_guitar_is_positive_class = True

guitar_function_df = pd.read_csv('./function_guitar.csv')
results_df = pd.read_csv('./results.csv')

def create_count_measures_file():
    annot_gp_dir_path = './data' # Repertoire contenant l'ensemble des .gp + nom_de_fichier_sans_gp/Score.gpif
    title_list = []
    artist_list = []
    part_name_list = []
    measure_count_list = []
    row_list = []
    columns = ['Artist','Title','Part','Measures']

    for file_name in sorted(os.listdir(annot_gp_dir_path)):
        if (file_name.endswith('.gp')):
            print(file_name)
            s = ParserGP().parseFile(annot_gp_dir_path+"/"+file_name.replace('.gp','')+'/score.gpif')
            title = s.metadata.title
            print('title : {}'.format(title))
            title_list.append(title)
            artist = s.metadata.getContributorsByRole('artist')[0].name
            print('artist : {}'.format(artist))
            artist_list.append(artist)
            part_name = s.getElementsByClass(stream.Part)[0].partName
            print('part : {}'.format(part_name))
            part_name_list.append(part_name)
            measure_count = len(s.getElementsByClass(stream.Part)[0].getElementsByClass(stream.Measure))
            print('nombre de mesures : {}'.format(measure_count))
            measure_count_list.append(measure_count)
            row_list.append([artist,title,part_name,measure_count])


    df = pd.DataFrame(np.array(row_list),columns=columns)
    df.to_csv('count_measures.csv')

count_measures_file = './count_measures.csv'
if not os.path.isfile(count_measures_file):
    create_count_measures_file()

measure_number_df = pd.read_csv(count_measures_file)

def get_fixed_length_int_string(n,l):
    s=str(n)
    for i in range(0,l-len(str(n))):
        s+=" "
    return s

def get_confusion_matrix_analysis_string(TP,FP,FN,TN):
    # Attention : garder en tête que les labels Rhythm Guitar sont les 0 (N) (et non les 1 (P))
    tpr = TP/(TP+FN) # non-rhythm guitar recall
    tnr = TN/(TN+FP) # rhythm guitar recall
    bal_acc = (tpr+tnr)/2
    rhythm_recall = tnr
    rhythm_precision = TN/(TN+FN)
    rhythm_f1 = 2*rhythm_precision*rhythm_recall/(rhythm_precision+rhythm_recall)
    non_rhythm_recall = tpr
    non_rhythm_precision = TP/(TP+FP)
    n_rhythm_f1 = 2*non_rhythm_precision*non_rhythm_recall/(non_rhythm_precision+non_rhythm_recall)
    s = "       |\nTP="+get_fixed_length_int_string(TP,4)+"| FP="+get_fixed_length_int_string(FP,4)+"\n       |\n----------------\n       |\nFN="+get_fixed_length_int_string(FN,4)+"| TN="+get_fixed_length_int_string(TN,4)+"\n       |\n\n"
    s+='balanced accuracy = {0:.2f}\n'.format(bal_acc)
    s+='positive class = rhythm guitar (negative = other)\n'
    s+='recall (~variety) = {0:.2f}\n'.format(rhythm_recall)
    s+='precision (~consistence) = {0:.2f}\n'.format(rhythm_precision)
    # s+='non r.g recall (~variety) = {0:.2f}\n'.format(non_rhythm_recall)
    # s+='non r.g precision (~consistence) = {0:.2f}\n'.format(non_rhythm_precision)
    s+='F1 score = {0:.2f}\n'.format(rhythm_f1)
    # s+='non r.g F1 score = {0:.2f}\n'.format(n_rhythm_f1)
    # s+='\n("r.g" means "rhythm guitar")\n'
    return s

def display_evaluation(treshold = 0.5, rnn = 1):

    P = [] #Prédictions
    E = [] #Etiquettes

    cr = csv.reader(open(predicted_labels_file_path, "r", newline="",encoding='utf8'))
    for ligne in cr:
        for i in range(1,len(ligne)):
            P.append(ligne[i])
    cr = csv.reader(open(reference_labels_file_path, "r", newline="",encoding='utf8'))
    for ligne in cr:
        for i in range(1,len(ligne)):
            E.append(ligne[i])

    print(P[0])

    tp = 0
    fp = 0
    tn = 0
    fn = 0

    for k in range(len(P)):
        pred = float(P[k])
        etiq = float(E[k])

        if pred > treshold:
            if etiq == 1:
                tp+=1
            else :
                fp+=1
        else :
            if etiq == 1:
                fn+=1
            else :
                tn+=1

    print('tp={} fp={} fn={} tn={}'.format(tp,fp,fn,tn))
    print(get_confusion_matrix_analysis_string(tp,fp,fn,tn))    # le fait que les rhythm guitar sont les 0 et non les 1 est pris en compte dans get_confusion_matrix_analysis_string

def display_cat(treshold,type):
    results_df = pd.read_csv("./results.csv")
    print('DISPLAY {}'.format(type))
    for index, row in results_df.iterrows():
        file = row['Fichier'].split('/')[-2]
        mesure = row['Mesure']
        ref = float(row['Ref'])
        pred = float(row['Pred'])
        if rhythm_guitar_is_positive_class:
            if type=="TN" and pred > treshold and ref == 1:
                print('{} mesure {}'.format(file,mesure))
            if type=="TP" and pred <= treshold and ref == 0:
                print('{} mesure {}'.format(file,mesure))
            if type=="FN" and pred > treshold and ref == 0:
                print('{} mesure {} pred:{}'.format(file,mesure,pred))
            if type=="FP" and pred <= treshold and ref == 1:
                print('{} mesure {} pred:{}'.format(file,mesure,pred))
        else :
            if type=="TP" and pred > treshold and ref == 1:
                print('{} mesure {}'.format(file,mesure))
            if type=="TN" and pred <= treshold and ref == 0:
                print('{} mesure {}'.format(file,mesure))
            if type=="FP" and pred > treshold and ref == 0:
                print('{} mesure {} pred:{}'.format(file,mesure,pred))
            if type=="FN" and pred <= treshold and ref == 1:
                print('{} mesure {} pred:{}'.format(file,mesure,pred))
    print('END {}'.format(type))

def get_ref_seg_list(song_title,song_artist,song_part_name):
    file_m_df = measure_number_df.loc[(measure_number_df['Title'] == song_title) & (measure_number_df['Artist'] == song_artist) & (measure_number_df['Part'] == song_part_name)]
    assert file_m_df.shape[0] == 1 , 'file not precisely found : {}'.format(file_m_df)
    measure_number = int(file_m_df.iloc[0]['Measures'])
    format_name = song_title+'\n'+song_artist
    file_df = guitar_function_df.loc[(guitar_function_df['Track_Name'] == format_name) & (guitar_function_df['Partie'] == song_part_name)]
    nm = file_df.shape[0]
    seg_list = ['e' for i in range(measure_number)]
    for index, row in file_df.iterrows():
        m = int(row['Mesure'])
        acc = int(row['Acc'])
        if acc == 1:
            seg_list[m-1]='r'
        else :
            assert int(row['Solo'])==1, 'acc et solo must be exclusive {}'.format(row)
            seg_list[m-1]='l'
    return seg_list

def get_pred_seg_list(song_title,song_artist,song_part_name):
    file_m_df = measure_number_df.loc[(measure_number_df['Title'] == song_title) & (measure_number_df['Artist'] == song_artist) & (measure_number_df['Part'] == song_part_name)]
    assert file_m_df.shape[0] == 1 , 'file not precisely found : {}'.format(file_m_df)
    measure_number = int(file_m_df.iloc[0]['Measures'])
    format_name = song_title+'\n'+song_artist
    file_df = results_df.loc[(guitar_function_df['Track_Name'] == format_name) & (guitar_function_df['Partie'] == song_part_name)]
    nm = file_df.shape[0]
    seg_list = ['e' for i in range(measure_number)]
    for index, row in file_df.iterrows():
        m = int(row['Mesure'])
        pred = float(row['Pred'])
        if pred <= 0.5:
            seg_list[m-1]='r'
        else :
            seg_list[m-1]='l'
    return seg_list

def get_sec_durations(label_list,label):
    current_section_label = 'e'
    current_section_length = 0
    sec_durations = []
    for i in range(len(label_list)):
        l = label_list[i]
        if l == current_section_label or l == 'e':
            current_section_length += 1
            continue
        else :
            if current_section_label == label:
                sec_durations.append(current_section_length)
            current_section_label = l
            current_section_length = 1
    if current_section_label == label:
        sec_durations.append(current_section_length)
    return sec_durations


def display_continuity_evaluation():

    ref_sec_durations = []
    pred_sec_durations = []
    nb_rg_measures_ref = 0
    nb_rg_measures_pred = 0
    for index, row in measure_number_df.iterrows():
        song_artist = row['Artist']
        song_title = row['Title']
        song_part_name = row['Part']
        # print('{} - {} - {}'.format(song_artist,song_title,song_part_name))
        desc = song_artist+' - '+song_title+' ('+song_part_name+')'
        ref_seg_list = get_ref_seg_list(song_title,song_artist,song_part_name)
        nb_rg_measures_ref += ref_seg_list.count('r')
        pred_seg_list = get_pred_seg_list(song_title,song_artist,song_part_name)
        nb_rg_measures_pred += pred_seg_list.count('r')
        # print('ref_seg_list = {}'.format(ref_seg_list))
        # print('pred_seg_list = {}'.format(pred_seg_list))
        tab_ref_sec_durations = get_sec_durations(ref_seg_list,'r')
        # print(tab_ref_sec_durations)
        ref_sec_durations.extend(tab_ref_sec_durations)
        # get_sec_durations(ref_seg_list,'l')
        tab_pred_sec_durations = get_sec_durations(pred_seg_list,'r')
        # print(tab_pred_sec_durations)
        pred_sec_durations.extend(tab_pred_sec_durations)
        # get_sec_durations(pred_seg_list,'l')

    print('ref_sec_durations = {}'.format(ref_sec_durations))

    print('pred_sec_durations = {}'.format(pred_sec_durations))
    print('number of rg measures annotated : {}'.format(nb_rg_measures_ref))
    print('number of rg sections annotated : {}'.format(len(ref_sec_durations)))
    print('mean rg section length in annotations : {}'.format(round(mean(ref_sec_durations),1)))
    print('number of isolated measures in annotations : {}'.format(ref_sec_durations.count(1)))
    print('number of rg measures predicted : {}'.format(nb_rg_measures_pred))
    print('number of rg sections predicted : {}'.format(len(pred_sec_durations)))
    print('mean rg section length in predictions : {}'.format(round(mean(pred_sec_durations),1)))
    print('number of isolated measures in predictions : {}'.format(pred_sec_durations.count(1)))

display_evaluation(treshold = 0.5)
# display_cat(0.5,"FP") # will display all the False Positives (resp False Negatives, etc.)
display_continuity_evaluation()