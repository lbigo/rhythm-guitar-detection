# rhythm-guitar-detection

Code accompanying ISMIR 2021 paper "Identification of rhythm guitar sections in symbolic tablatures".

    @inproceedings{regnier2021identification,
      title={Identification of rhythm guitar sections in symbolic tablatures},
      author={R{\'e}gnier, David and Martin, Nicolas and Bigo, Louis},
      booktitle={International Society for Music Information Retrieval Conference (ISMIR 2021)},
      year={2021}
    }


# Running the code

## Data

The source data of this work are Guitar Pro files (`.gp`). Guitar Pro files should be located in the directory `data`. A `.gp` file is nothing more than a zipped file. Running `unzip` to a `.gp` file will produce a directory including a file `score.gpif` that includes all the tablature information in an XML like format.

In order to run the present code, the directory `data` needs to include all the `.gp` files, and for each `.gp` file a directory having exactly the same name but without the extension `.gp`. Each directory must include a file `score.gpif`. All file/dir names should not include any blankspace. For example, the `data` directory will include

* `3_doors_down-kryptonite-02-A.Guitar.gp` (a `.gp` file)
* `3_doors_down-kryptonite-02-A.Guitar` (a directory that include a `score.gpif` file)

## Parsing manual annotations and computing bar-level features from `.gp` files

Manual annotations should be added in Guitar Pro with two possible labels: `fun:acc` and `fun:solo`.
The file `function_guitar.csv` will be created by running `python3 extraction-features.py`. It will indicate for each measure of the tablature the manual annotation (acc or solo) as well as the computed values for the 33 features.

The 100 `.gp` files of the dataset of this work are not released for copyright issue. However, the file `features-and-annotations.csv` includes the computed features, as well as the manual label annotations, for all the tablatures of the dataset.

## Evaluating the model

The evaluation is done in a Leave-one-piece-out cross validation process. Therefore, a distinct model is trained for the evaluation of each song of the annotated dataset. For each evaluation, the model is trained to predict manual annotations from computed features on the (n-1) files. Once it is trained, the model is used to predict manual annotations of file n from its computed features.

Running `python3 training.py` will train an LSTM model on all songs (excluding song i), store it in `model.h5` and evaluate this model on song i. Most of the LSTM parameters are set up in the function `training(i)`. The results of the predictions can be visualised in the file `results.csv`.

## Display evaluation metrics

Running `display_evaluation.py` will look at the file `results.csv` and compute recall, precision and F1 score. It will also display statistics on the continuity of the annotated vs predicted sections (for the later, it will need to read in `count_measures.csv` the number of bars in each file.).

## Use the model to predict annotations on an external dataset

On `prediction-on-external-dataset.py` set `ext_dataset_path` to the path of the directory that contains all the gp files (as `.gp` files and as uncompressed directories). Then run `prediction-on-external-dataset.py`. This will:

1. extract the features and write them in `function_guitar-ext.csv`.
2. build and train a model from the annotated dataset vectorised in `function_guitar.csv`
3. use the model to predict all bars in `function_guitar-ext.csv`. Predicted labels will be written (in an additional column) in `function_guitar-ext-with-labels.csv`